<?php

namespace Drupal\elfsight_instagram_widget\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * {@inheritdoc}
 */
class ElfsightInstagramWidgetController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    $url = 'https://apps.elfsight.com/embed/instalink/?utm_source=portals&utm_medium=drupal&utm_campaign=instagram-widget&utm_content=sign-up';

    require_once __DIR__ . '/embed.php';

    return [
      'response' => 1,
    ];
  }

}
